﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="BUILD" Type="Folder">
			<Item Name="Post-Build Action.vi" Type="VI" URL="../../../../VI_GENERIC/PostBuild/Post-Build Action.vi"/>
			<Item Name="Prompt_commit.vi" Type="VI" URL="../../../../VI_GENERIC/PostBuild/Prompt_commit.vi"/>
		</Item>
		<Item Name="TDL" Type="Folder">
			<Item Name="AILES" Type="Folder">
				<Item Name="TDL-AILES_OK.vi" Type="VI" URL="../TDL-AILES_OK.vi"/>
			</Item>
			<Item Name="ANTARES" Type="Folder">
				<Item Name="TDL-ANTARES_OK.vi" Type="VI" URL="../TDL-ANTARES_OK.vi"/>
			</Item>
			<Item Name="CASSIOPEE" Type="Folder">
				<Item Name="TDL-CASSIOPEE_OK.vi" Type="VI" URL="../TDL-CASSIOPEE_OK.vi"/>
			</Item>
			<Item Name="CRISTAL" Type="Folder">
				<Item Name="TDL-CRISTAL_OK.vi" Type="VI" URL="../TDL-CRISTAL_OK.vi"/>
			</Item>
			<Item Name="DEIMOS" Type="Folder">
				<Item Name="TDL-DEIMOS_OK.vi" Type="VI" URL="../TDL-DEIMOS_OK.vi"/>
			</Item>
			<Item Name="DESIRS" Type="Folder">
				<Item Name="TDL-DESIRS_OK.vi" Type="VI" URL="../TDL-DESIRS_OK.vi"/>
			</Item>
			<Item Name="DIFFABS" Type="Folder">
				<Item Name="TDL-DIFFABS_OK.vi" Type="VI" URL="../TDL-DIFFABS_OK.vi"/>
			</Item>
			<Item Name="DISCO" Type="Folder">
				<Item Name="TDL-DISCO_OK.vi" Type="VI" URL="../TDL-DISCO_OK.vi"/>
			</Item>
			<Item Name="GALAXIES" Type="Folder">
				<Item Name="TDL-GALAXIES_OK.vi" Type="VI" URL="../TDL-GALAXIES_OK.vi"/>
			</Item>
			<Item Name="HERMES" Type="Folder">
				<Item Name="TDL-HERMES_OK.vi" Type="VI" URL="../TDL-HERMES_OK.vi"/>
			</Item>
			<Item Name="LUCIA" Type="Folder">
				<Item Name="TDL-LUCIA_OK.vi" Type="VI" URL="../TDL-LUCIA_OK.vi"/>
			</Item>
			<Item Name="MARS" Type="Folder">
				<Item Name="TDL-MARS_OK.vi" Type="VI" URL="../TDL-MARS_OK.vi"/>
			</Item>
			<Item Name="METRO" Type="Folder">
				<Item Name="TDL-METRO_OK.vi" Type="VI" URL="../TDL-METRO_OK.vi"/>
			</Item>
			<Item Name="NANO-ANA" Type="Folder">
				<Item Name="TDL-NANO_TOMO_OK.vi" Type="VI" URL="../TDL-NANO_TOMO_OK.vi"/>
			</Item>
			<Item Name="ODE" Type="Folder">
				<Item Name="TDL-ODE_OK.vi" Type="VI" URL="../TDL-ODE_OK.vi"/>
			</Item>
			<Item Name="PLEIADES" Type="Folder">
				<Item Name="TDL-PLEIADES_OK.vi" Type="VI" URL="../TDL-PLEIADES_OK.vi"/>
			</Item>
			<Item Name="PSD" Type="Folder">
				<Item Name="TDL-D08-1_PSD.vi" Type="VI" URL="../TDL-D08-1_PSD.vi"/>
			</Item>
			<Item Name="PSICHE" Type="Folder">
				<Item Name="TDL-PSICHE_squeeze_test_.vi" Type="VI" URL="../TDL-PSICHE_squeeze_test_.vi"/>
			</Item>
			<Item Name="PUMA" Type="Folder">
				<Item Name="TDL-PUMA_OK.vi" Type="VI" URL="../TDL-PUMA_OK.vi"/>
			</Item>
			<Item Name="PX1" Type="Folder">
				<Item Name="TDL-PX1_OK.vi" Type="VI" URL="../TDL-PX1_OK.vi"/>
			</Item>
			<Item Name="PX2" Type="Folder">
				<Item Name="TDL-PX2_OK.vi" Type="VI" URL="../TDL-PX2_OK.vi"/>
			</Item>
			<Item Name="ROCK" Type="Folder">
				<Item Name="TDL-Rock_OK.vi" Type="VI" URL="../TDL-Rock_OK.vi"/>
			</Item>
			<Item Name="SAMBA" Type="Folder">
				<Item Name="TDL-SAMBA_OK.vi" Type="VI" URL="../TDL-SAMBA_OK.vi"/>
			</Item>
			<Item Name="SEXTANTS" Type="Folder">
				<Item Name="TDL-SEXTANTS_OK.vi" Type="VI" URL="../TDL-SEXTANTS_OK.vi"/>
			</Item>
			<Item Name="SIRIUS" Type="Folder">
				<Item Name="TDL-SIRIUS_OK.vi" Type="VI" URL="../TDL-SIRIUS_OK.vi"/>
			</Item>
			<Item Name="SIXS" Type="Folder">
				<Item Name="TDL-SIXS_OK.vi" Type="VI" URL="../TDL-SIXS_OK.vi"/>
			</Item>
			<Item Name="SMIS" Type="Folder">
				<Item Name="TDL-SMIS_OK.vi" Type="VI" URL="../TDL-SMIS_OK.vi"/>
			</Item>
			<Item Name="SWING" Type="Folder">
				<Item Name="TDL-SWING_OK.vi" Type="VI" URL="../TDL-SWING_OK.vi"/>
			</Item>
			<Item Name="TEMPO" Type="Folder">
				<Item Name="TDL-TEMPO_OK.vi" Type="VI" URL="../TDL-TEMPO_OK.vi"/>
			</Item>
			<Item Name="THz" Type="Folder">
				<Item Name="THz_OK.vi" Type="VI" URL="../THz_OK.vi"/>
			</Item>
			<Item Name="Code couleur_v2(SubVi).vi" Type="VI" URL="../Code couleur_v2(SubVi).vi"/>
		</Item>
		<Item Name="Page principale_TDL_maxcurrent.vi" Type="VI" URL="../Page principale_TDL_maxcurrent.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="COuleurs_tdl.vi" Type="VI" URL="../COuleurs_tdl.vi"/>
			<Item Name="Code couleur(SubVi).vi" Type="VI" URL="../Code couleur(SubVi).vi"/>
			<Item Name="Interlocks Status (SubVI)B.vi" Type="VI" URL="../Interlocks Status (SubVI)B.vi"/>
			<Item Name="Liste_DEB.vi" Type="VI" URL="../../Supervision_Anneau/Liste_DEB.vi"/>
			<Item Name="Liste_DIAPH.vi" Type="VI" URL="../../Supervision_Anneau/Liste_DIAPH.vi"/>
			<Item Name="Liste_ETUV.vi" Type="VI" URL="../../Supervision_Anneau/Liste_ETUV.vi"/>
			<Item Name="Liste_JPIR.vi" Type="VI" URL="../../Supervision_Anneau/Liste_JPIR.vi"/>
			<Item Name="Liste_Jauges.vi" Type="VI" URL="../../Supervision_Anneau/Liste_Jauges.vi"/>
			<Item Name="Liste_OCC.vi" Type="VI" URL="../../Supervision_Anneau/Liste_OCC.vi"/>
			<Item Name="Liste_PI.vi" Type="VI" URL="../../Supervision_Anneau/Liste_PI.vi"/>
			<Item Name="Liste_TC.vi" Type="VI" URL="../../Supervision_Anneau/Liste_TC.vi"/>
			<Item Name="Liste_VS.vi" Type="VI" URL="../../Supervision_Anneau/Liste_VS.vi"/>
			<Item Name="Nouvelle_Couleur_PRG.vi" Type="VI" URL="../Nouvelle_Couleur_PRG.vi"/>
			<Item Name="PLC Message (SubVI).vi" Type="VI" URL="../PLC Message (SubVI).vi"/>
			<Item Name="PLC Message Ligne(SubVI).vi" Type="VI" URL="../PLC Message Ligne(SubVI).vi"/>
			<Item Name="SQUEEZE.vi" Type="VI" URL="../SQUEEZE.vi"/>
			<Item Name="Text_Interlcok_blabla_subVi.vi" Type="VI" URL="../Text_Interlcok_blabla_subVi.vi"/>
			<Item Name="Text_Interlock_SUbVi.vi" Type="VI" URL="../Text_Interlock_SUbVi.vi"/>
			<Item Name="_DServerClientRequest.ctl" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerClientRequest.ctl"/>
			<Item Name="_DServerClientRequestData.ctl" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerClientRequestData.ctl"/>
			<Item Name="_DServerClientRequestType.ctl" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerClientRequestType.ctl"/>
			<Item Name="_DServerDevicePropertyToBoolean.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToBoolean.vi"/>
			<Item Name="_DServerDevicePropertyToDouble.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToDouble.vi"/>
			<Item Name="_DServerDevicePropertyToDoubleArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToDoubleArray.vi"/>
			<Item Name="_DServerDevicePropertyToFloat.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToFloat.vi"/>
			<Item Name="_DServerDevicePropertyToFloatArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToFloatArray.vi"/>
			<Item Name="_DServerDevicePropertyToLong.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToLong.vi"/>
			<Item Name="_DServerDevicePropertyToLongArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToLongArray.vi"/>
			<Item Name="_DServerDevicePropertyToShort.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToShort.vi"/>
			<Item Name="_DServerDevicePropertyToShortArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToShortArray.vi"/>
			<Item Name="_DServerDevicePropertyToString.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToString.vi"/>
			<Item Name="_DServerDevicePropertyToStringArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToStringArray.vi"/>
			<Item Name="_DServerDevicePropertyToULong.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToULong.vi"/>
			<Item Name="_DServerDevicePropertyToUShort.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerDevicePropertyToUShort.vi"/>
			<Item Name="_DServerEvent.ctl" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerEvent.ctl"/>
			<Item Name="_DServerEventCreate.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerEventCreate.vi"/>
			<Item Name="_DServerGetRawDeviceProperty.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerGetRawDeviceProperty.vi"/>
			<Item Name="_DServerReplyToExecuteCommand.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerReplyToExecuteCommand.vi"/>
			<Item Name="_DServerReplyToReadAttribute.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerReplyToReadAttribute.vi"/>
			<Item Name="_DServerRequestDataFromDevBoolean.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevBoolean.vi"/>
			<Item Name="_DServerRequestDataFromDevCharArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevCharArray.vi"/>
			<Item Name="_DServerRequestDataFromDevDouble.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevDouble.vi"/>
			<Item Name="_DServerRequestDataFromDevDoubleArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevDoubleArray.vi"/>
			<Item Name="_DServerRequestDataFromDevDoubleStringArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevDoubleStringArray.vi"/>
			<Item Name="_DServerRequestDataFromDevEnum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevEnum.vi"/>
			<Item Name="_DServerRequestDataFromDevFloat.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevFloat.vi"/>
			<Item Name="_DServerRequestDataFromDevFloatArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevFloatArray.vi"/>
			<Item Name="_DServerRequestDataFromDevLong.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevLong.vi"/>
			<Item Name="_DServerRequestDataFromDevLong64.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevLong64.vi"/>
			<Item Name="_DServerRequestDataFromDevLong64Array.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevLong64Array.vi"/>
			<Item Name="_DServerRequestDataFromDevLongArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevLongArray.vi"/>
			<Item Name="_DServerRequestDataFromDevLongStringArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevLongStringArray.vi"/>
			<Item Name="_DServerRequestDataFromDevShort.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevShort.vi"/>
			<Item Name="_DServerRequestDataFromDevShortArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevShortArray.vi"/>
			<Item Name="_DServerRequestDataFromDevState.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevState.vi"/>
			<Item Name="_DServerRequestDataFromDevString.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevString.vi"/>
			<Item Name="_DServerRequestDataFromDevStringArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevStringArray.vi"/>
			<Item Name="_DServerRequestDataFromDevULong.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevULong.vi"/>
			<Item Name="_DServerRequestDataFromDevULong64.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevULong64.vi"/>
			<Item Name="_DServerRequestDataFromDevULong64Array.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevULong64Array.vi"/>
			<Item Name="_DServerRequestDataFromDevULongArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevULongArray.vi"/>
			<Item Name="_DServerRequestDataFromDevUShort.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevUShort.vi"/>
			<Item Name="_DServerRequestDataFromDevUShortArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataFromDevUShortArray.vi"/>
			<Item Name="_DServerRequestDataToBooleanImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToBooleanImage.vi"/>
			<Item Name="_DServerRequestDataToBooleanScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToBooleanScalar.vi"/>
			<Item Name="_DServerRequestDataToBooleanSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToBooleanSpectrum.vi"/>
			<Item Name="_DServerRequestDataToDevBoolean.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevBoolean.vi"/>
			<Item Name="_DServerRequestDataToDevCharArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevCharArray.vi"/>
			<Item Name="_DServerRequestDataToDevDouble.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevDouble.vi"/>
			<Item Name="_DServerRequestDataToDevDoubleArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevDoubleArray.vi"/>
			<Item Name="_DServerRequestDataToDevDoubleStringArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevDoubleStringArray.vi"/>
			<Item Name="_DServerRequestDataToDevEnum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevEnum.vi"/>
			<Item Name="_DServerRequestDataToDevFloat.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevFloat.vi"/>
			<Item Name="_DServerRequestDataToDevFloatArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevFloatArray.vi"/>
			<Item Name="_DServerRequestDataToDevLong.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevLong.vi"/>
			<Item Name="_DServerRequestDataToDevLong64.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevLong64.vi"/>
			<Item Name="_DServerRequestDataToDevLong64Array.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevLong64Array.vi"/>
			<Item Name="_DServerRequestDataToDevLongArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevLongArray.vi"/>
			<Item Name="_DServerRequestDataToDevLongStringArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevLongStringArray.vi"/>
			<Item Name="_DServerRequestDataToDevShort.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevShort.vi"/>
			<Item Name="_DServerRequestDataToDevShortArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevShortArray.vi"/>
			<Item Name="_DServerRequestDataToDevState.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevState.vi"/>
			<Item Name="_DServerRequestDataToDevString.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevString.vi"/>
			<Item Name="_DServerRequestDataToDevStringArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevStringArray.vi"/>
			<Item Name="_DServerRequestDataToDevULong.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevULong.vi"/>
			<Item Name="_DServerRequestDataToDevULong64.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevULong64.vi"/>
			<Item Name="_DServerRequestDataToDevULong64Array.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevULong64Array.vi"/>
			<Item Name="_DServerRequestDataToDevULongArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevULongArray.vi"/>
			<Item Name="_DServerRequestDataToDevUShort.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevUShort.vi"/>
			<Item Name="_DServerRequestDataToDevUShortArray.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDevUShortArray.vi"/>
			<Item Name="_DServerRequestDataToDoubleImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDoubleImage.vi"/>
			<Item Name="_DServerRequestDataToDoubleScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDoubleScalar.vi"/>
			<Item Name="_DServerRequestDataToDoubleSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToDoubleSpectrum.vi"/>
			<Item Name="_DServerRequestDataToEnumScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToEnumScalar.vi"/>
			<Item Name="_DServerRequestDataToFloatImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToFloatImage.vi"/>
			<Item Name="_DServerRequestDataToFloatScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToFloatScalar.vi"/>
			<Item Name="_DServerRequestDataToFloatSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToFloatSpectrum.vi"/>
			<Item Name="_DServerRequestDataToLong64Image.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToLong64Image.vi"/>
			<Item Name="_DServerRequestDataToLong64Scalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToLong64Scalar.vi"/>
			<Item Name="_DServerRequestDataToLong64Spectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToLong64Spectrum.vi"/>
			<Item Name="_DServerRequestDataToLongImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToLongImage.vi"/>
			<Item Name="_DServerRequestDataToLongScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToLongScalar.vi"/>
			<Item Name="_DServerRequestDataToLongSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToLongSpectrum.vi"/>
			<Item Name="_DServerRequestDataToShortImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToShortImage.vi"/>
			<Item Name="_DServerRequestDataToShortScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToShortScalar.vi"/>
			<Item Name="_DServerRequestDataToShortSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToShortSpectrum.vi"/>
			<Item Name="_DServerRequestDataToStateImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToStateImage.vi"/>
			<Item Name="_DServerRequestDataToStateScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToStateScalar.vi"/>
			<Item Name="_DServerRequestDataToStateSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToStateSpectrum.vi"/>
			<Item Name="_DServerRequestDataToStringImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToStringImage.vi"/>
			<Item Name="_DServerRequestDataToStringScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToStringScalar.vi"/>
			<Item Name="_DServerRequestDataToStringSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToStringSpectrum.vi"/>
			<Item Name="_DServerRequestDataToUCharImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToUCharImage.vi"/>
			<Item Name="_DServerRequestDataToUCharScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToUCharScalar.vi"/>
			<Item Name="_DServerRequestDataToUCharSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToUCharSpectrum.vi"/>
			<Item Name="_DServerRequestDataToULong64Image.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToULong64Image.vi"/>
			<Item Name="_DServerRequestDataToULong64Scalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToULong64Scalar.vi"/>
			<Item Name="_DServerRequestDataToULong64Spectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToULong64Spectrum.vi"/>
			<Item Name="_DServerRequestDataToULongImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToULongImage.vi"/>
			<Item Name="_DServerRequestDataToULongScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToULongScalar.vi"/>
			<Item Name="_DServerRequestDataToULongSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToULongSpectrum.vi"/>
			<Item Name="_DServerRequestDataToUShortImage.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToUShortImage.vi"/>
			<Item Name="_DServerRequestDataToUShortScalar.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToUShortScalar.vi"/>
			<Item Name="_DServerRequestDataToUShortSpectrum.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestDataToUShortSpectrum.vi"/>
			<Item Name="_DServerRequestToString.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestToString.vi"/>
			<Item Name="_DServerRequestTypeToString.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerRequestTypeToString.vi"/>
			<Item Name="_DServerSetBooleanImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetBooleanImageAttributeValue.vi"/>
			<Item Name="_DServerSetBooleanScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetBooleanScalarAttributeValue.vi"/>
			<Item Name="_DServerSetBooleanSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetBooleanSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetDoubleImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetDoubleImageAttributeValue.vi"/>
			<Item Name="_DServerSetDoubleScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetDoubleScalarAttributeValue.vi"/>
			<Item Name="_DServerSetDoubleSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetDoubleSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetEnumScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetEnumScalarAttributeValue.vi"/>
			<Item Name="_DServerSetFloatImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetFloatImageAttributeValue.vi"/>
			<Item Name="_DServerSetFloatScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetFloatScalarAttributeValue.vi"/>
			<Item Name="_DServerSetFloatSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetFloatSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetLong64ImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetLong64ImageAttributeValue.vi"/>
			<Item Name="_DServerSetLong64ScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetLong64ScalarAttributeValue.vi"/>
			<Item Name="_DServerSetLong64SpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetLong64SpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetLongImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetLongImageAttributeValue.vi"/>
			<Item Name="_DServerSetLongScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetLongScalarAttributeValue.vi"/>
			<Item Name="_DServerSetLongSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetLongSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetShortImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetShortImageAttributeValue.vi"/>
			<Item Name="_DServerSetShortScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetShortScalarAttributeValue.vi"/>
			<Item Name="_DServerSetShortSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetShortSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetStateImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetStateImageAttributeValue.vi"/>
			<Item Name="_DServerSetStateScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetStateScalarAttributeValue.vi"/>
			<Item Name="_DServerSetStateSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetStateSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetStringImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetStringImageAttributeValue.vi"/>
			<Item Name="_DServerSetStringScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetStringScalarAttributeValue.vi"/>
			<Item Name="_DServerSetStringSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetStringSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetUCharImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetUCharImageAttributeValue.vi"/>
			<Item Name="_DServerSetUCharScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetUCharScalarAttributeValue.vi"/>
			<Item Name="_DServerSetUCharSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetUCharSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetULong64ImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetULong64ImageAttributeValue.vi"/>
			<Item Name="_DServerSetULong64ScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetULong64ScalarAttributeValue.vi"/>
			<Item Name="_DServerSetULong64SpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetULong64SpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetULongImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetULongImageAttributeValue.vi"/>
			<Item Name="_DServerSetULongScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetULongScalarAttributeValue.vi"/>
			<Item Name="_DServerSetULongSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetULongSpectrumAttributeValue.vi"/>
			<Item Name="_DServerSetUShortImageAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetUShortImageAttributeValue.vi"/>
			<Item Name="_DServerSetUShortScalarAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetUShortScalarAttributeValue.vi"/>
			<Item Name="_DServerSetUShortSpectrumAttributeValue.vi" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DServerSetUShortSpectrumAttributeValue.vi"/>
			<Item Name="_DataType.ctl" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DataType.ctl"/>
			<Item Name="_DeviceProperty.ctl" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_DeviceProperty.ctl"/>
			<Item Name="_GenericDataBuffer.ctl" Type="VI" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/_GenericDataBuffer.ctl"/>
			<Item Name="tango.lvlib" Type="Library" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_vi.llb/tango.lvlib"/>
			<Item Name="tango_binding.so" Type="Document" URL="/usr/Local/soleil-root/bindings/labview-16.0/vis/tango_binding.so"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="SupervisionTDL" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{A4C65115-7167-11E4-BDEC-001EC9B5D419}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A4C64EB3-7167-11E4-BDEC-001EC9B5D419}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{949B8FC5-8E54-11E7-89C1-B38499203CC0}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SupervisionTDL</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/home/ApplisLabview/UV/Binaires/Linux/Supervision_TDL</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/BUILD/Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{949B91C6-8E54-11E7-89C1-B43A123B5924}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Destination[0].destName" Type="Str">SupervisionTDL.lvapp</Property>
				<Property Name="Destination[0].path" Type="Path">/home/ApplisLabview/UV/Binaires/Linux/Supervision_TDL/SupervisionTDL.lvapp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/home/ApplisLabview/UV/Binaires/Linux/Supervision_TDL/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="SourceCount" Type="Int">31</Property>
				<Property Name="Source[0].itemID" Type="Str">{B2159813-33BB-11EB-A7C6-C61445DC214A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/TDL/METRO/TDL-METRO_OK.vi</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/TDL/DIFFABS/TDL-DIFFABS_OK.vi</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/TDL/SAMBA/TDL-SAMBA_OK.vi</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/TDL/DESIRS/TDL-DESIRS_OK.vi</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/TDL/GALAXIES/TDL-GALAXIES_OK.vi</Property>
				<Property Name="Source[14].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[14].type" Type="Str">VI</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/TDL/HERMES/TDL-HERMES_OK.vi</Property>
				<Property Name="Source[15].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[15].type" Type="Str">VI</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/TDL/SIRIUS/TDL-SIRIUS_OK.vi</Property>
				<Property Name="Source[16].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[16].type" Type="Str">VI</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/TDL/SIXS/TDL-SIXS_OK.vi</Property>
				<Property Name="Source[17].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[17].type" Type="Str">VI</Property>
				<Property Name="Source[18].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[18].itemID" Type="Ref">/My Computer/TDL/SWING/TDL-SWING_OK.vi</Property>
				<Property Name="Source[18].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[18].type" Type="Str">VI</Property>
				<Property Name="Source[19].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[19].itemID" Type="Ref">/My Computer/TDL/ANTARES/TDL-ANTARES_OK.vi</Property>
				<Property Name="Source[19].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[19].type" Type="Str">VI</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/TDL/PUMA/TDL-PUMA_OK.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[20].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[20].itemID" Type="Ref">/My Computer/TDL/CASSIOPEE/TDL-CASSIOPEE_OK.vi</Property>
				<Property Name="Source[20].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[20].type" Type="Str">VI</Property>
				<Property Name="Source[21].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[21].itemID" Type="Ref">/My Computer/TDL/PX1/TDL-PX1_OK.vi</Property>
				<Property Name="Source[21].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[21].type" Type="Str">VI</Property>
				<Property Name="Source[22].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[22].itemID" Type="Ref">/My Computer/TDL/DEIMOS/TDL-DEIMOS_OK.vi</Property>
				<Property Name="Source[22].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[22].type" Type="Str">VI</Property>
				<Property Name="Source[23].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[23].itemID" Type="Ref">/My Computer/TDL/LUCIA/TDL-LUCIA_OK.vi</Property>
				<Property Name="Source[23].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[23].type" Type="Str">VI</Property>
				<Property Name="Source[24].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[24].itemID" Type="Ref">/My Computer/TDL/TEMPO/TDL-TEMPO_OK.vi</Property>
				<Property Name="Source[24].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[24].type" Type="Str">VI</Property>
				<Property Name="Source[25].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[25].itemID" Type="Ref">/My Computer/TDL/CRISTAL/TDL-CRISTAL_OK.vi</Property>
				<Property Name="Source[25].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[25].type" Type="Str">VI</Property>
				<Property Name="Source[26].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[26].itemID" Type="Ref">/My Computer/TDL/SEXTANTS/TDL-SEXTANTS_OK.vi</Property>
				<Property Name="Source[26].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[26].type" Type="Str">VI</Property>
				<Property Name="Source[27].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[27].itemID" Type="Ref">/My Computer/TDL/PX2/TDL-PX2_OK.vi</Property>
				<Property Name="Source[27].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[27].type" Type="Str">VI</Property>
				<Property Name="Source[28].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[28].itemID" Type="Ref">/My Computer/TDL/SMIS/TDL-SMIS_OK.vi</Property>
				<Property Name="Source[28].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[28].type" Type="Str">VI</Property>
				<Property Name="Source[29].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[29].itemID" Type="Ref">/My Computer/TDL/AILES/TDL-AILES_OK.vi</Property>
				<Property Name="Source[29].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[29].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/TDL/Code couleur_v2(SubVi).vi</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[30].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[30].itemID" Type="Ref">/My Computer/TDL/PSD/TDL-D08-1_PSD.vi</Property>
				<Property Name="Source[30].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[30].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Page principale_TDL_maxcurrent.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/TDL/NANO-ANA/TDL-NANO_TOMO_OK.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/TDL/ROCK/TDL-Rock_OK.vi</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/TDL/ODE/TDL-ODE_OK.vi</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/TDL/PSICHE/TDL-PSICHE_squeeze_test_.vi</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/TDL/MARS/TDL-MARS_OK.vi</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/TDL/PLEIADES/TDL-PLEIADES_OK.vi</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="TgtF_fileDescription" Type="Str">SupervisionTDL</Property>
				<Property Name="TgtF_internalName" Type="Str">SupervisionTDL</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright 2014 </Property>
				<Property Name="TgtF_productName" Type="Str">SupervisionTDL</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{A4C66FF1-7167-11E4-BDEC-001EC9B5D419}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">SupervisionTDL.lvapp</Property>
			</Item>
		</Item>
	</Item>
</Project>
